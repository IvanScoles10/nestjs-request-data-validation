import { NestFactory } from '@nestjs/core';
import { ValidationError, ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { ValidationException } from './filters/validation.exception';
import { ValidationFilter } from './filters/validation.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // api pre-fix
  app.setGlobalPrefix('api/v1');
  // custom-validation
  app.useGlobalFilters(
      new ValidationFilter()
  )
  app.useGlobalPipes(new ValidationPipe({
      skipMissingProperties: false,
      exceptionFactory: (errors: ValidationError[]) => {
          const messages = errors.map((error) => {
              return {
                  error: `${error.property} has wrong value ${error.value}.`,
                  message: Object.values(error.constraints).join(''),
              }
          })

          return new ValidationException(messages);
      }

  }));
  await app.listen(3000);
}
bootstrap();
