import { Module } from '@nestjs/common';
import { ProductController } from 'src/product/product.controller'

@Module({
    imports: [],
    controllers: [ProductController],
    providers: []
})
export class ProductModule { }
