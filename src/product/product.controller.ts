import { Controller, Body, Post } from '@nestjs/common';
import { ProductDto } from 'src/product/dto/product.dto';

@Controller('product')
export class ProductController {

    @Post()
    async createProduct(@Body() product: ProductDto) {
        return product;
    }
}