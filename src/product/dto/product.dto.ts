import { IsMongoId, IsNotEmpty, IsOptional, IsString, IsNumber, ArrayNotEmpty } from 'class-validator';
import { Image } from 'src/types/type';

export class ProductDto {
    @IsMongoId()
    @IsOptional()
    _id: string;

    @IsString()
    @IsNotEmpty()
    uid: string

    @IsString()
    @IsNotEmpty()
    product_name: string;

    @IsString()
    @IsNotEmpty()
    product_description: string;

    @IsString()
    @IsNotEmpty()
    product_brand: string;

    @IsNumber()
    @IsNotEmpty()
    product_quantity: number;

    @IsNumber()
    @IsNotEmpty()
    product_quantity_sold: number

    @IsNumber()
    @IsNotEmpty()
    product_quantity_available: number;

    @IsString()
    @IsNotEmpty()
    product_price: string;

    @ArrayNotEmpty()
    product_images: Image[]

    @ArrayNotEmpty()
    product_category: string[]

    created_at: Date

    updated_at: Date
}