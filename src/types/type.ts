export interface Image {
    id: number
}

export interface Product {
    _id?: string;
    product_images: Image[]
    product_quantity_sold: number
    product_category: string[]
    uid: string
    product_name: string
    product_brand: string
    product_description: string
    product_quantity: number
    product_quantity_available: number
    product_price: string
    created_at: Date
    updated_at?: Date
}

export interface Address {
    _id?: string;
    address_additional_info: string;
    address_location: string;
    address_province: string;
    address_street: string;
    address_street_number: string;
    address_zip_code: string;
    uid: string;
    created_at: Date
    updated_at?: Date
}

export interface Company {
    _id?: string;
    uid: string;
    company_description: string;
    company_name: string;
    company_phone: string;
    created_at: Date
    updated_at?: Date
}

export interface User {
    _id?: string;
    uid: string;
    first_name: string;
    last_name: string;
    type: string;
    created_at: Date
    updated_at?: Date
}

export interface Sort {
    created_at?: number
    product_quantity_sold?: number
}

export interface Paginate {
    page: number,
    perPage: number,
    sort: Sort
}